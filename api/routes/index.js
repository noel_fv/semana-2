'use strict'

const express = require('express');
const cors = require('cors');
const dishController = require('./dishRouter');
const promoController = require('../controllers/promoRouter');
const leaderController = require('../controllers/leaderRouter');
const api = express();


api.use(cors()); //Alow all cors request


api.get('/dishes', dishController.get);
api.post('/dishes', dishController.save);
api.put('/dishes', dishController.update);
api.delete('/dishes', dishController.remove);
api.get('/dishes/:dishId', dishController.findByID);
api.put('/dishes/:dishId', dishController.update);
api.delete('/dishes/:dishId', dishController.remove);

api.get('/promotions', promoController.get);
api.post('/promotions', promoController.save);
api.put('/promotions', promoController.update);
api.delete('/promotions', promoController.remove);
api.get('/promotions/:promoId', promoController.findByID);
api.put('/promotions/:promoId', promoController.update);
api.delete('/promotions/:promoId', promoController.remove);

api.get('/leaders', leaderController.get);
api.post('/leaders', leaderController.save);
api.put('/leaders', leaderController.update);
api.delete('/leaders', leaderController.remove);
api.get('/leaders/:leaderId', leaderController.findByID);
api.put('/leaders/:leaderId', leaderController.update);
api.delete('/leaders/:leaderId', leaderController.remove);


module.exports = api;