'use strict'

const _ = require('underscore');

function get(req, res) {
  res.send("Hello promotions");
};

function findByID(req, res) {
    let promoId = req.params.promoId;
    res.json({
      ok: true,
      promoId
    });

  };


  function update(req, res) {
    let promoId = req.params.promoId;
  
    if(promoId=== undefined){
      res.statusCode=403;
      res.send("Operacion PUT no soportada en promotions");
    }else{
      res.json({
        ok: true,
        promoId
      });
    }
  
  };
  
  function remove(req, res) {
    let promoId = req.params.promoId;
  
    if(promoId=== undefined){
      res.send("delete all promotions");
    }else{
      res.json({
        ok: true,
        message: "Delete id " +promoId
      });
    }
  };

  function save(req, res) {
    res.send("agregando un promotion " + req.body.name);
  };

  module.exports = {
    get,
    findByID,
    update,
    remove,
    save
};