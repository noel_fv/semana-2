// ============================
//  Puerto
// ============================
process.env.PORT = process.env.PORT || 3000;

// ============================
//  IP
// ============================
process.env.IP = process.env.IP || '0.0.0.0'


// ============================
//  Entorno
// ============================
process.env.NODE_ENV = process.env.NODE_ENV || 'dev';

// ============================
//  Nivel de log
// ============================
process.env.LOG_LEVEL = process.env.LOG_LEVEL || 'debug'
