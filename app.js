'use strict'

const express = require('express');
const validator = require('express-validator');
const bodyParser = require('body-parser'); 
//const api = require('./api/routes');
const morgan = require('morgan');
const mongoose = require('mongoose');
const Dishes = require('./api/models/dishes');
require('./config');
const dishRouter = require('./api/routes/dishRouter');


const app = express();
const connect = mongoose.connect('mongodb://localhost:27017/conFusion');

app.use(validator());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(morgan(process.env.NODE_ENV));
app.use('/', dishRouter);


app.listen(process.env.PORT, process.env.IP,() => {
    console.log( 'API REST SERVER RUNNING .... PORT= ' + process.env.PORT);
});

 
connect.then((db) => {
    console.log("CONNECTED MONGODB SUCCESS!!!");
}, (err) => { console.log(err); });


module.exports=app;